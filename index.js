const Twitter = require("twitter");
const keywords = require("./keywords.json");

require("dotenv").config();
const {
  TWITTER_API_KEY,
  TWITTER_CONSUMER_SECRET,
  ACCESS_TOKEN_KEY,
  ACCESS_TOKEN_SECRET,
} = process.env;

// Create twitter client
const client = new Twitter({
  consumer_key: TWITTER_API_KEY,
  consumer_secret: TWITTER_CONSUMER_SECRET,
  access_token_key: ACCESS_TOKEN_KEY,
  access_token_secret: ACCESS_TOKEN_SECRET,
});

// Streaming api listening to "fire and rehire"
const stream = client.stream("statuses/filter", { track: keywords.join(",") });

stream.on("data", (event, err) => {
  // If new tweet containing keyword
  if (!err) {
    const { id_str } = event;
    // Retweet it
    client.post(`statuses/retweet/${id_str}`, (err) => {
      if (err && 'message' in err[0]) console.error(`Error retweeting: ${err[0].message}`);
      else if (err) console.error(`Error without message! ${err[0]}`);
    });
  } else {
    console.error(err);
  }
});

stream.on("err", function(error) {
  console.error(`Stream returned error: ${error}`);
  throw error;
});

