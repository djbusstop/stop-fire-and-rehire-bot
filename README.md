# Stop Fire and Rehire Bot

## Development

To set up, copy the file `.env_template` to `.env` and input the Twitter API credentials.

Install dependencies with `npm i`

To run, enter the command `npm run start`.
